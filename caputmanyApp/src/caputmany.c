#include <linux/limits.h>
#include <pthread.h>
#include <sched.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <cadef.h>
#include <epicsStdlib.h>

#define TIMEOUT 1.0
#define PV_LEN 50

typedef struct
{
    int sleep;
    char name[PV_LEN];
} push_request_t;

#include <sys/time.h>
typedef long long milliepoch;

static inline milliepoch current_timestamp()
{
    struct timeval te;
    gettimeofday(&te, NULL); // get current time
    milliepoch milliseconds = te.tv_sec * 1000LL + te.tv_usec / 1000; // calculate milliseconds
    return milliseconds;
}

void* push(void* arg)
{
    push_request_t* request = (push_request_t*)arg;
    int result;
    chanId chid;
    milliepoch next = current_timestamp(), sleep;
    double* dbuf = calloc(1, sizeof(double));
    printf("%s\n", request->name);
    result = ca_context_create(ca_enable_preemptive_callback);
    if (result != ECA_NORMAL) {
        printf("%d: ChannelId %s - Status %s\n", result, request->name, ca_message(result));
    }

    result = ca_create_channel(request->name, NULL, NULL, CA_PRIORITY_DEFAULT, &chid);
    if (ECA_NORMAL != result) {
        printf("ca_create_channel: %s, status %s\n", request->name, ca_message(result));
    }

    result = ca_pend_io(TIMEOUT);
    if (result != ECA_NORMAL) {
        printf("ca_pend_io: %s, status %s\n", request->name, ca_message(result));
    }

    for (double i = 0;; i++) {
        next = next + request->sleep;
        *dbuf = i;
        result = ca_put(DBR_DOUBLE, chid, dbuf);
        if (result != ECA_NORMAL) {
            printf("%d: ChannelId %s- Value %f - Status %s\n", result, request->name, i, ca_message(result));
        }
        result = ca_pend_io(TIMEOUT);
        if (result != ECA_NORMAL) {
            printf("%d: ChannelId %s- Value %f - Status %s\n", result, request->name, i, ca_message(result));
        }
        sleep = next - current_timestamp();
        if (sleep > 0) {
            usleep(sleep * 1e3);
        }
    }

    ca_context_destroy();
    return NULL;
}

int main(int argc, char* argv[])
{
    int opt;
    int nbPvs = 0, sleep = 100;
    char pvs_path[PATH_MAX];
    char buffer[PV_LEN];
    pthread_t* threads = NULL;
    push_request_t* requests = NULL;
    pthread_attr_t thread_attr;
    struct sched_param schedparams;
    int result;

    while ((opt = getopt(argc, argv, "f:n:s:")) != -1) {
        switch (opt) {
        case 'f':
            strncpy(pvs_path, optarg, PATH_MAX - 1);
            break;
        case 'n':
            nbPvs = atoi(optarg);
            break;
        case 's':
            sleep = atoi(optarg);
            break;
        default:
            break;
        }
    }

    printf("filename %s, nb %d\n", pvs_path, nbPvs);
    printf("Initializing...\n");

    requests = calloc(nbPvs, sizeof(push_request_t));

    FILE* file = fopen(pvs_path, "r");
    for (int i = 0; i < nbPvs; i++) {
        fgets(buffer, PV_LEN, file);
        buffer[strlen(buffer) - 1] = '\0';
        requests[i].sleep = sleep;
        strncpy(requests[i].name, buffer, PV_LEN);
    }
    fclose(file);

    threads = calloc(nbPvs, sizeof(pthread_t));

    result = pthread_attr_init(&thread_attr);
    printf("pthread_attr_init: %d\n", result);

    result = pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);
    printf("pthread_attr_setinheritsched: %d\n", result);

    result = pthread_attr_setschedpolicy(&thread_attr, SCHED_FIFO);
    printf("pthread_attr_setschedpolicy: %d\n", result);

    schedparams.sched_priority = sched_get_priority_max(SCHED_FIFO);
    result = pthread_attr_setschedparam(&thread_attr, &schedparams);
    printf("pthread_attr_setschedparam: %d\n", result);

    printf("Running Threads...\n");
    for (int i = 0; i < nbPvs; i++) {
        result = pthread_create(&threads[i], &thread_attr, push, &requests[i]);
        printf("%d\n", result);
    }

    for (int i = 0; i < nbPvs; i++) {
        pthread_join(threads[i], NULL);
    }

    pthread_attr_destroy(&thread_attr);
    free(requests);
    free(threads);
    return 0;
}